var User = require('../../model/user.js');

var user = new User();

var NewRoomController = function(request, response) {
	this.request = request;
	this.response = response;
}

NewRoomController.prototype.execute = function() {
	var request = this.request;
	var response = this.response;
	var body = "";

	request.on('data', function(datas) {
		body += datas;
	});

	request.on('end', function() {
		try {
			bodyParsed = JSON.parse(body);

			user.find(['authToken'], [bodyParsed.authToken], function(data) {
				global.rooms.push({
					id: global.rooms.length + 1,
					name: bodyParsed.name,
					language: data.language,
					players: 0,
					busy: false
				});
				response.writeHead(200);
				response.end();
			});
		} 
		catch(e) {
			response.writeHead(500);
		  	response.end();
		}
	});
};

module.exports = NewRoomController;