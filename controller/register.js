var mysql = require('mysql'),
User = require('../model/user.js');

var RegisterController = function(request, response) {
	this.request = request;
	this.response = response;
}

RegisterController.prototype.execute = function() {
	var request = this.request;
	var response = this.response;
	var body = "";

	request.on('data', function(datas) {
		body += datas;
	});

  	request.on('end', function() {
        try {
			bodyParsed = JSON.parse(body);
			var user = new User();
			var passwordMatch = bodyParsed.password == bodyParsed.passwordConfirmation ? true : false;

			if (!passwordMatch) {
				response.writeHead(400);
				response.end();
			}

			user.find(['username'], [bodyParsed.username], function(data) {
				if (!data) {
        			user.create(bodyParsed, function(authToken) {
        				response.writeHead(200, {
							'Set-Cookie': 'authToken='+authToken
						});
						response.end();
        			});
				}
				else {
    				response.writeHead(401);
    				response.end();
				}
			})     
        } 
        catch(e) {
          response.end();
        }
  	});
};

module.exports = RegisterController;