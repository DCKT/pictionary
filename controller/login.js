var mysql = require('mysql'),
User = require('../model/user.js');

var LoginController = function(request, response) {
	this.request = request;
	this.response = response;
}

LoginController.prototype.execute = function() {
	var request = this.request;
	var response = this.response;
	var body = "";

	request.on('data', function(datas) {
		body += datas;
	});

	request.on('end', function() {
		try {
			data = JSON.parse(body);
			var username = data.username;
			var password = data.password;
			var user = new User();

			user.find(['username', 'password'], [username, password], function(data){
				if (data.length == 0) {
					response.writeHead(401);
					response.end();
				}
				else {
        			user.updateAuthToken(username, function(authToken) {
        				response.writeHead(200, {
							'Set-Cookie': 'authToken='+authToken
						});
						response.end();
        			});
				}
			});
		} 
		catch(e) {
			response.end();
		}
	});
};

module.exports = LoginController;