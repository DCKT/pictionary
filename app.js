var http = require('http'),
Router = require('./router.js'),
socket = require('socket.io'),
User = require('./model/user.js');

global.rooms = [{
	id: 1,
	name: "Room de test",
	language: "FR",
	players: 4,
	busy: false
}];

var routes = [
	"/", 
	"/login", 
	"/register"
];

var routesUserConnected = [
	"/",
	"/profile",
	"/rooms",
	"/rooms/create",
	"/test"
];

function parseCookies (request) {
    var list = {},
        rc = request.headers.cookie;

    rc && rc.split(';').forEach(function( cookie ) {
        var parts = cookie.split('=');
        list[parts.shift().trim()] = unescape(parts.join('='));
    });

    return list;
}

var user = new User();

var app = http.createServer(function(request, response) {
	var cookies = parseCookies(request);
	var router = new Router(request, response, cookies);

	if (cookies.authToken == undefined || cookies.authToken == null) {
		router.map(routes);
	}
	else {
		user.find(['authToken'], [cookies.authToken], function(data){
			data ? router.map(routesUserConnected) : router.map(routes); 
		});
		
	}
}).listen(3000);

var z = 0;
var users = {};
var messages = [];

var io = socket.listen(app);

io.sockets.on('connection', function (socket) {

	for (var user in users) {
		socket.emit('new-player', users[user]);
	}

	for (var mess in messages) {
		socket.emit('messages', messages[mess]);
	}

	for (var room in global.rooms) {
		socket.emit('room', global.rooms[room]);
	}
	
	socket.on('new-player', function (){
    	users[currentUser.id] = global.currentUser;
    	io.sockets.emit('new-player', global.currentUser);
  	});

  	socket.on('messages', function (data) {
		var message = {};
	    message.username = global.currentUser.username;
	    message.message = data;
	    messages.push(message);

	    if (messages.length > 5) {
	        messages.shift();
	    };

	    io.sockets.emit('messages', message);
  	});

  	socket.on('draw', function(data) {
  		socket.broadcast.emit('draw', data);
  	});
  	
  	socket.on('disconnect', function(){
  		if (!global.currentUser) {
  			return false;
  		}
    	delete users[global.currentUser.id];
    	io.sockets.emit('disconnect', global.currentUser);
	});

	socket.on('draw', function(data) {
		socket.broadcast.emit('draw');
	});
});


console.log("\nDON'T FORGET TO CHANGE IP VALUE FOR SOCKET.IO !!!!!!!!!!!!!!!");