var mysql = require('mysql');

var connection = mysql.createConnection({
  host     : 'localhost',
  user     : 'root',
  password : '',
  database : 'pictionary'
});

connection.connect();;

function generateAuthToken() {
	var authToken = "";
	var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";

	for( var i=0; i < 10; i++ ) {
		authToken += possible.charAt(Math.floor(Math.random() * possible.length));
	}
	return authToken;
}

var User = function() {
};

User.prototype.findAll = function() {
	connection.query("SELECT * FROM users", function(err, rows, fields) {
		return rows[0];
	});
};

User.prototype.find = function(conditions, parameters, callback) {
	var sql = "SELECT * FROM users WHERE ";
	for (var i = 0; i < conditions.length; i++) {
		sql += conditions[i]+" = ?";
		if (conditions.length > 1 && i < (conditions.length - 1)) {
			sql += " AND ";
		}
	}

	var inserts = parameters;
	sql = mysql.format(sql, inserts);

	connection.query(sql, function(err, rows, fields) {
		callback(rows[0]);
	});

};

User.prototype.updateAuthToken = function(username, callback) {
	var authToken = generateAuthToken();
	var sql = "UPDATE users SET authToken = ? WHERE username = ?";
	var inserts = [authToken, username];
	sql = mysql.format(sql, inserts);

	connection.query(sql, function(err, rows, fields) {
		callback(authToken);
	});
};

User.prototype.create = function(user, callback) {
	var authToken = generateAuthToken();

	var sql = "INSERT INTO users (username, password, language, authToken) VALUES (?,?,?, ?)";
	var inserts = [user.username, user.password, user.language, authToken];
	sql = mysql.format(sql, inserts);


	connection.query(sql, function(err, rows, fields) {
		callback(authToken);
	});
};
module.exports = User;