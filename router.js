var fs = require('fs'),
mysql = require('mysql');

var Router = function(request, response, cookies) {
	this.request = request;
	this.response = response;
	this.cookies = cookies;
};

Router.prototype.map = function(routes) {
	var requestUrl = this.request.url;
	var extensionJS = requestUrl.slice(requestUrl.length - 3, requestUrl.length);
	var extensionCSS = requestUrl.slice(requestUrl.length - 4, requestUrl.length);

	var notFound = true;

	for(var i = 0; i < routes.length; i++) {
		if (routes[i] == this.request.url) {
			this.route(routes[i]);
			notFound = false;
		}
		else if (extensionCSS == ".css" || extensionJS == ".js") {
			this.route(requestUrl);
			notFound = false;
		}
	}

	if (notFound) {
		this.route("/404");
	}
};

Router.prototype.route = function(url) {
	var response = this.response;
	var request = this.request;
	var method = request.method;
	var requestUrl = request.url;

	if (method == "POST") {
		if (requestUrl == url) {
			var path = "./controller/";
			path += url.length > 1 ? url.slice(1, url.length) : "index";
			path += ".js";

			var Controller = require(path);
			var routeController = new Controller(request, response);
			routeController.execute();
		}
	}
	else if (method == "GET") {
		var extensionCSS = requestUrl.slice(requestUrl.length - 4, requestUrl.length);
		var extensionJS = requestUrl.slice(requestUrl.length - 3, requestUrl.length);

		if (extensionCSS == ".css" || extensionJS == ".js") {
			var path = "assets/";
			path += extensionCSS == ".css" ? "stylesheets" : "";
			path += extensionJS == ".js" ? "scripts" : "";
			path += requestUrl;

			fs.readFile(path, function(err, data) {
				response.end(data);
			});
		}
		else if (requestUrl == url) {
			if (this.cookies.authToken != undefined) {
				var path = "templates/userConnected";
			}
			else {
				var path = "templates";
			}
			path += url.length > 1 ? requestUrl : "/index";
			path += ".html"
			
			fs.readFile(path, function(err, data) {
				response.end(data);
			});
		}
		else if (url == "/404") {
			fs.readFile('templates/404.html', function(err, data) {
				response.end(data);
			});
		}
	}
}

module.exports = Router;