var canvas, ctx, flag = false,
    prevX = 0,
    currX = 0,
    prevY = 0,
    currY = 0,
    dot_flag = false;

var x = "black",
    y = 2;

function init() {
    hud = document.getElementById('hud');
    hud.style.width = "400px";
    canvas = document.getElementById('canvas');
    ctx = canvas.getContext("2d");
    canvas.width = window.screen.availWidth - 400;
    w = canvas.width;
    canvas.height = window.screen.availHeight;
    h = canvas.height;


    var socket = io.connect('http://192.168.1.10:3000');
    var messagesList = document.getElementById('messages-list');
    var input = document.getElementById('input-message');

    canvas.addEventListener("mousemove", function (e) {
        findxy('move', e);
    }, false);
    canvas.addEventListener("mousedown", function (e) {
        findxy('down', e);
    }, false);
    canvas.addEventListener("mouseup", function (e) {
        findxy('up', e);
    }, false);
    canvas.addEventListener("mouseout", function (e) {
        findxy('out', e);
    }, false);


    socket.emit('new-player');

    socket.on('new-player', function (user) {
        var userList = document.getElementById('user-list');
        userList.innerHTML += "<li class='list-group-item' id='"+user.id+"'><span class='badge'>0</span>"+user.username; 
        messagesList.innerHTML += "<li><p class='text-info'><em>"+ user.username +" join the room.</em></li>";
    });

    socket.on('messages', function (data) {
        messagesList.innerHTML += "<li><strong>"+data.username+" :</strong> "+data.message+"</li>";
    });

    
    input.addEventListener('keypress', function (e) {
        if (e.keyCode == 13) {
            var message = input.value;
            socket.emit('messages', message);
            input.value = '';
        }
    });

    socket.on('disconnect', function (user) {
        var div = document.getElementById(user.id);
        div.parentNode.removeChild(div);
        messagesList.innerHTML += "<li><p class='text-danger'><em>"+ user.username +" left the room.</em></li>";
    });
}


function color(obj) {
    x = obj.id;

    if (x == "white") {
        x = "#ecf0f1";
        y = 14;
    } 
    else {
        y = 2;
    }
}

function draw() {
    ctx.beginPath();
    ctx.moveTo(prevX, prevY);
    ctx.lineTo(currX, currY);
    ctx.strokeStyle = x;
    ctx.lineWidth = y;
    ctx.stroke();
    ctx.closePath();
}

function erase() {
    var clean = confirm("Clear the page ?");
    if (clean) {
        ctx.clearRect(0, 0, w, h);
    }
}

function findxy(res, e) {
    if (res == 'down') {
        prevX = currX;
        prevY = currY;
        currX = e.clientX - canvas.offsetLeft;
        currY = e.clientY - canvas.offsetTop;

        flag = true;
        dot_flag = true;
        if (dot_flag) {
            ctx.beginPath();
            ctx.fillStyle = x;
            ctx.fillRect(currX, currY, 2, 2);
            ctx.closePath();
            dot_flag = false;
        }
    }
    if (res == 'up' || res == "out") {
        flag = false;
    }
    if (res == 'move') {
        if (flag) {
            prevX = currX;
            prevY = currY;
            currX = e.clientX - canvas.offsetLeft;
            currY = e.clientY - canvas.offsetTop;
            draw();
        }
    }
}