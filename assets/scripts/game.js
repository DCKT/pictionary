window.onload = function() {
    var socket = io.connect('http://localhost:3000');
    var ctx;
    var penColor = "#FF0000";
    var messagesList = document.getElementById('messages-list');
    var input = document.getElementById('input-message');

    var colors = document.getElementsByClassName('color');

    for (var i = 0; i < colors.length; i++) {
        colors[i].addEventListener('click', function(e) {
            e.preventDefault();

            penColor = this.dataset.color;
        });
    }
    document.getElementById('blue').addEventListener('click', function(e) {
        e.preventDefault();
        penColor = document.getElementById('blue').dataset.color;
    });

    document.getElementById('clr').addEventListener('click', function(e) {
        e.preventDefault();
        ctx.clearRect(0, 0, window.screen.availWidth - 400, window.screen.availHeight);
    });

    socket.on('draw', function(data) {
        ctx.fillCircle(data[0], data[1], data[2], data[3]);
    });
    socket.emit('new-player');

    socket.on('new-player', function (user) {
        var userList = document.getElementById('user-list');
        userList.innerHTML += "<li class='list-group-item' id='"+user.id+"'><span class='badge'>0</span>"+user.username; 
        messagesList.innerHTML += "<li><p class='text-info'><em>"+ user.username +" join the room.</em></li>";
    });

    socket.on('messages', function (data) {
        messagesList.innerHTML += "<li><strong>"+data.username+" :</strong> "+data.message+"</li>";
    });

    
    input.addEventListener('keypress', function (e) {
        if (e.keyCode == 13) {
            var message = input.value;
            socket.emit('messages', message);
            input.value = '';
        }
    });

    socket.on('disconnect', function (user) {
        var div = document.getElementById(user.id);
        div.parentNode.removeChild(div);
        messagesList.innerHTML += "<li><p class='text-danger'><em>"+ user.username +" left the room.</em></li>";
    });
    
    function createCanvas(parent, width, height) {
        var canvas = {};
        canvas.node = document.createElement('canvas');
        canvas.context = canvas.node.getContext('2d');
        canvas.node.width = width;
        canvas.node.height = height;
        parent.appendChild(canvas.node);
        return canvas;
    }

    function init(container, width, height, fillColor) {
        var canvas = createCanvas(container, width, height);
        ctx = canvas.context;
        ctx.fillCircle = function(x, y, radius, fillColor) {
            this.fillStyle = fillColor;
            this.beginPath();
            this.moveTo(x, y);
            this.arc(x, y, radius, 0, Math.PI * 2, false);
            this.fill();
        };
        ctx.clearTo = function(fillColor) {
            ctx.fillStyle = fillColor;
            ctx.fillRect(0, 0, width, height);
        };
        ctx.clearTo(fillColor);

        // bind mouse events
        canvas.node.onmousemove = function(e) {
            if (!canvas.isDrawing) {
               return;
            }
            var x = e.pageX - this.offsetLeft;
            var y = e.pageY - this.offsetTop;
            var radius = 10; // or whatever
            var fillColor = penColor;
            ctx.fillCircle(x, y, radius, fillColor);

            socket.emit('draw', [x, y, radius, fillColor]);
        };
        canvas.node.onmousedown = function(e) {
            canvas.isDrawing = true;
        };
        canvas.node.onmouseup = function(e) {
            canvas.isDrawing = false;
        };
    }

    var container = document.getElementById('canvas');
    init(container, window.screen.availWidth - 400, window.screen.availHeight, '#ddd');

}